<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    //Definiendo el nombre de la tabla en caso de tener un nombre distinto al del modelo
    //protected $table = 'categorias';
    //Definiendo el nombre de la clave principal en caso de tener un nombre diferente a id
    //protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'descripcion', 'condicion'];

    public function articulos()
    {
        return $this->hasMany('App\Articulo');
    }
}

