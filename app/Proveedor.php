<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    //Se especifica la tabla con la que se asocia al modelo
    protected $table = 'proveedores';

    //Se definen los campos que pueden ser llenados mediante formularios
    protected $fillable = [
        'id', 'contacto', 'telefono_contacto'
    ];
    //Se deshabilitan los timestamps(created_at, updated_at)
    public $timestamps = FALSE;

    //Se establece la relacion con la tabla persona
    public function persona()
    {
        return $this->belongsTo('App\Persona');
    }
}
